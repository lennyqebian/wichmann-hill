package wichmannhill;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author leonardoquevedo
 */
public class RandomGenerator {

    protected long X_0;

    protected long Y_0;

    protected long Z_0;

    protected String noDecimals = "%.0f";
    
    RandomGenerator(long X_0, long Y_0, long Z_0) {
        this.X_0 = X_0;
        this.Y_0 = Y_0;
        this.Z_0 = Z_0;
    }

    public double[] random(int n) throws Exception {
        if (!(n > 0)) {
            throw new Exception("¡La cantidad de números aleatorios a generar debe ser mayor que cero (0)!");
        }
        ArrayList<Long> X = new ArrayList();
        ArrayList<Long> Y = new ArrayList();
        ArrayList<Long> Z = new ArrayList();
        double[] U = new double[n];
        long x, y, z;
        X.add(this.X_0);
        Y.add(this.Y_0);
        Z.add(this.Z_0);

        for (int i = 0; i < n; i++) {
            x = (171 * X.get(i)) % 30269;
            y = (171 * Y.get(i)) % 30307;
            z = (171 * Z.get(i)) % 30323;
            X.add(x);
            Y.add(y);
            Z.add(z);
            U[i] = (x/30269.0 + y/30307.0 + z/30323.0) % 1.0;
        }
        return U;
    }

    public String randomStr(int n, int decimalPlaces) throws Exception {
        double[] U = this.random(n);
        String format;

        if (!(decimalPlaces > 1)) {
            throw new Exception("¡El número de dígitos decimales debería ser por lo menos 2!");
        }
        format = "%." + decimalPlaces + "f";

        if (U.length > 1) {
            List<String> output = Arrays.stream(U).boxed().map((aDouble) -> {
                return String.format(format, aDouble);
            }).collect(Collectors.toList());
            return "Números aleatorios generados: \n\n"
                    + String.join("\n", output);
        }
        return "Número aleatorio generado: " + String.format(format, U[0]);
    }

    public String randomStr(int n) throws Exception {
        return randomStr(n, 4);
    }

    public double random() throws Exception {
        return this.random(1)[0];
    }
}
